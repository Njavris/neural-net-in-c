#ifndef __NEURALNET_H_
#define __NEURALNET_H_

#include <list>
#include <utility>
#include <vector>
#include <string>
#include <cmath>
#include <utility>
#include "Layer.h"

#include <iostream>

class ActivationFunctions {
public:
	virtual ~ActivationFunctions(void) {};
	virtual double actFn(double x) { return 0.0f; };
	virtual double dActFn(double x) { return 0.0f; };
};

class Sigmoid : public ActivationFunctions { 
public:
	double actFn(double x) { return 1.0f / (1.0f + exp(-x)); };
	double dActFn(double x) { return x * (1.0f - x); };
};

class ReLu : public ActivationFunctions {
public:
	double actFn(double x) { return (x <= 0.0f) ? 0.0f : x; };
	double dActFn(double x) { return (x <= 0.0f) ? 0.0f : 1.0f; };
};

class ReLuLeaky : public ActivationFunctions {
public:
	double actFn(double x) { return (x < 0.0f) ? 0.001f * x : x; };
	double dActFn(double x) { return (x <= 0.0f) ? 0.0f : 1.0f; };
};

class TrainingSet {
	int nrIn;
	int nrOut;
	std::vector<std::pair<double *, double *>> entries;
public:
	TrainingSet(int in, int out) : nrIn(in), nrOut(out) {
	};
	~TrainingSet() {
		for (int i = 0; i < (int)entries.size(); i++) {
			delete []entries[i].first;
			delete []entries[i].second;
		}
	};
	int size(void) { return (int)entries.size(); };
	int getIn(void) { return nrIn; };
	int getOut(void) { return nrOut; };
	std::pair<double *, double *> *getEntry(int idx) {
		if (idx < 0 || idx >= (int)entries.size())
			return nullptr;
		return &entries[idx];
	};
	void push_back(double *inputs, double *outputs) {
		entries.push_back(std::make_pair(inputs, outputs));
	};
};

class NeuralNet {
	int inputCount;
	int outputCount;
	bool linked;
	double lr;
	std::list<Layer *> layers;
	ActivationFunctions *fn;
	void resetLayers(void) {
		for (std::list<Layer *>::iterator it = layers.begin();
				it != layers.end(); it++)
			(*it)->resetLayer();
	};
public:
	NeuralNet(int nrInputs, int nrOutputs, std::string act, double lr) :
			inputCount(nrInputs), outputCount(nrOutputs),
			linked(false), lr(lr) {
		if (!act.compare("Sigmoid"))
			fn = new Sigmoid();
		else if (!act.compare("ReLuLeaky"))
			fn = new ReLuLeaky();
		else if (!act.compare("ReLu"))
			fn = new ReLu();
		else
			fn = new ActivationFunctions();
	};
	~NeuralNet(void) { delete fn; };
	void addLayer(Layer *l) { this->layers.push_back(l); };
	int getInputCount(void) { return inputCount; };
	int getOutputCount(void) { return outputCount; };
	double getLR(void) { return lr; };
	double actFn(double x) { return fn->actFn(x); };
	double dActFn(double x) { return fn->dActFn(x); };
	double *compute(double *inputs);
	void adjustWeights(double *expectedOut);
	void linkLayers(void);
	void train(TrainingSet *set, int epochs);
	void test(TrainingSet *set);
	void dumpLayers(void) {
		for (std::list<Layer *>::iterator it = layers.begin();
				it != layers.end(); it++)
			(*it)->dumpLayer();
	}
};

#endif
