#ifndef __LAYER_H__
#define __LAYER_H__

#include <array>
#include "util.h"

class NeuralNet;

class Layer {
protected:
	NeuralNet *net;
	int size;
	double *inputs;
	double *outputs;
public:
	Layer(NeuralNet *net, int sz) : net(net), size(sz) {
		outputs = new double[sz];
	};
	~Layer() {
		delete []outputs;
	}
	double *getOutputs(void) { return outputs; };
	virtual void setInputs(double *inputs) { this->inputs = inputs; };
	virtual void forwardPropagate(Layer *prev, Layer *next) {};
	virtual void backwardPropagate(Layer *prev,
					Layer *next, double *out) {};
	virtual void resetLayer(void) {
		for (int i = 0; i < size; i++)
			outputs[i] = 0.0f;
	};
	int getSize(void) { return size; };
	virtual void dumpLayer(void) {};
};

class FullyConnectedLayer : public Layer {
	double *biases;
	double *weights;
	bool enDeltas;
	double *deltas;
	int inSz;
public:
	FullyConnectedLayer(NeuralNet *net, int inputSz, int sz);
	~FullyConnectedLayer(void);
	void forwardPropagate(Layer *prev, Layer *next);
	void backwardPropagate(Layer *prev, Layer *next, double *out);
	void setDeltasCalc(bool en);
	double *getDeltas(void) { return enDeltas ? deltas : nullptr; };
	double *getWeights(void) { return weights; };
	double *getBiases(void) { return biases; };
	void resetLayer(void) {
		for (int i = 0; i < size; i++) {
			outputs[i] = 0.0f;
			if (deltas)
				deltas[i] = 0.0f;
		}
	};
	void dumpLayer(void) {
		std::cout << "FC: sz:" << size;
		std::cout << " wts(bss): ";
		for (int i = 0; i < size; i++)
			std::cout << weights[i] << "(" << biases[i] << ") ";
		std::cout << std::endl;
	};
};

class InputLayer : public Layer {
public:
	InputLayer(NeuralNet *net, int sz) : Layer(net, sz) {};
	void setInputs(double *inputs) {
		for (int i = 0; i < size; i++)
			outputs[i] = inputs[i];
	};
	void dumpLayer(void) {
		std::cout << "In: sz:" << size << std::endl;
	};
};

class ConvolutionalLayer : public Layer {
	int width;
	int height;
	int fWidth;
	int fHeight;
	int channels;
	int kernelSz;
	int kernels;
	int hStrides;
	int wStrides;
	double *biases;
	double *weights;
	double *getKernel(int idx) {
			return &weights[kernelSz * kernelSz * channels]; };
	double calcFeature(int xOff, int yOff, int zOff);
public:
	ConvolutionalLayer(NeuralNet *net, int inWidth, int inHeight,
			int inChannels, int kernelSz,
			int wStrides, int hStrides);
	~ConvolutionalLayer();
	void forwardPropagate(Layer *prev, Layer *next);
	void backwardPropagate(Layer *prev, Layer *next, double *out);
	int getFeatureMapWidth(void) { return fWidth; };
	int getFeatureMapHeight(void) { return fHeight; };
	void dumpLayer(void) {
		std::cout << "CL: sz:" << size << std::endl;
	};
};
#endif
