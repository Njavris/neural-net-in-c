#include "Layer.h"
#include "NeuralNet.h"
#include "util.h"

/*
[Ii]	[Hj]
[I0]	[H0]	[Ok]
[I1]	[H1]	[O0]
[I2]	[H2]	[O1]
[I3]	[H3]	[O2]
...	...	...
[In]	[Hm]	[Ol]

Weights:
wH[j * n + i]
wO[k * m + j]
*/

FullyConnectedLayer::FullyConnectedLayer(NeuralNet *net, int inputSz, int sz) :
		Layer(net, sz) {
	inSz = inputSz;
	enDeltas = false;
	biases = new double[sz];
	weights = new double[sz * inSz];
	for (int i = 0; i < sz * inSz; i++) {
		if (i < sz)
			biases[i] = 0.0f;
		weights[i] = util::rand(-0.5f, 0.5f) *
			sqrt(1.0f / (double)(sz * inSz));
	}
	for (int i = 0; i < sz; i++)
		biases[i] = net->getLR() / 2.0f;
}

FullyConnectedLayer::~FullyConnectedLayer(void) {
	delete []biases;
	delete []weights;
}

void FullyConnectedLayer::setDeltasCalc(bool en) {
	if (en != enDeltas) {
		if (en) {
			deltas = new double[size];
			for (int i = 0; i < size; i++)
				deltas[i] = 0.0f;
		} else {
			delete []deltas;
		}
	}
	enDeltas = en;
}

void FullyConnectedLayer::forwardPropagate(Layer *prev, Layer *next) {
	for (int i = 0; i < size; i++) {
		double act = biases[i];
		for (int j = 0; j < inSz; j++)
			act += weights[i * inSz + j] * inputs[j];
		outputs[i] = net->actFn(act);
	}
}

void FullyConnectedLayer::backwardPropagate(Layer *prev, Layer *next,
		double *out) {
	FullyConnectedLayer *fcnext =
			static_cast<FullyConnectedLayer*>(next);
	double *nextDeltas = next ? fcnext->getDeltas() : nullptr;
	double *nextWeights = next ? fcnext->getWeights() : nullptr;
	double lr = net->getLR();

	if (!next && !out)
		return;

	if (!enDeltas)
		setDeltasCalc(true);

	for (int i = 0; i < size; i++) {
		double delta = 0.0f;
		if (out && !next) { // output layer
			delta = out[i] - outputs[i];
		} else { // hidden layer
			for (int j = 0; j < fcnext->getSize(); j++)
				delta += nextDeltas[j] *
					nextWeights[j * size + i];
		}
		deltas[i] = delta * net->dActFn(outputs[i]);
	}

	for (int i = 0; i < size; i++) {
		biases[i] += deltas[i] * lr;
		for (int j = 0; j < inSz; j++)
			weights[i * inSz + j] += inputs[j] * deltas[i] * lr;
	}
}

/*
[X0Y0]	[X1Y0]	...	[XmY0]
[X0Y1]	[X1Y1]	...	[XmY1]
[X0Y2]	[X1Y2]	...	[XmY2]
 ...	 ...	...	 ...
[X0Yn]	[X1Yn]	...	[XmYn]

inputs[(Yi * m + Xj) * n + CHk]
weights[kernel * ((krYi * krm + krXj) * chan + CHk)]
*/

ConvolutionalLayer::ConvolutionalLayer(NeuralNet *net,
		int inWidth, int inHeight, int inChannels, int kernelSz,
		int wStrides, int hStrides)
		: Layer(net, inWidth * inHeight) {
	int zeroPad = 0;
	width = inWidth;
	height = inHeight;
	fWidth = (width - kernelSz + 2 * zeroPad) / wStrides + 1;
	fHeight = (height - kernelSz + 2 * zeroPad) / hStrides + 1;
	channels = inChannels;
	this->kernelSz = kernelSz;
	this->wStrides = wStrides;
	this->hStrides = hStrides;
	biases = new double[fWidth * fHeight * channels];
	weights = new double[fWidth * fHeight * channels *
				kernelSz * kernelSz];
}

ConvolutionalLayer::~ConvolutionalLayer() {
	delete []biases;
	delete []weights;
}

double ConvolutionalLayer::calcFeature(int xOff, int yOff, int zOff) {
	double ret = 0.0f;
	int idx = ((yOff / hStrides) * fWidth + (xOff / wStrides)) *
					channels + zOff;
	double *kern = getKernel(idx);
	for (int x = xOff; x < (xOff + kernelSz); x++)
	for (int y = yOff; y < (yOff + kernelSz); y++)
		ret += inputs[(y * width + x) * channels + zOff] *
			kern[((y - yOff) * kernelSz + (x - xOff)) *
			channels + zOff] + biases[idx];
	return ret;
}

void ConvolutionalLayer::forwardPropagate(Layer *prev, Layer *next) {
	for (int x = 0; x < fWidth; x += wStrides)
	for (int y = 0; y < height; y += hStrides)
	for (int z = 0; z < channels; z++)
		outputs[((y * width) + x) * height + z] = 
			calcFeature(x, y, z);
}

void ConvolutionalLayer::backwardPropagate(Layer *prev,
			Layer *next, double *out) {
}

