#include <vector>
#include "NeuralNet.h"

void train_logic_function(std::string func, std::string act, std::vector<int> layers, int epochs, double lr) {
	unsigned i = 0;
	TrainingSet set = TrainingSet(2, 1);
	NeuralNet nn(2, 1, act, lr);
	nn.addLayer(new InputLayer(&nn, 2));

	for (; i < layers.size(); i++)
		nn.addLayer(new FullyConnectedLayer(&nn, i ? layers[i - 1] : 2, layers[i]));
	nn.addLayer(new FullyConnectedLayer(&nn, layers[i - 1], 1));

	std::cout << func << " with " << act << std::endl;
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			double *inputs = new double[2];
			double *outputs = new double[1]; 

			inputs[0] = (double)i;
			inputs[1] = (double)j;
			if (!func.compare("xor")) {
				outputs[0] = (!!i) ^ (!!j);
			} else if (!func.compare("or")) {
				outputs[0] = (!!i) | (!!j);
			} else if (!func.compare("and")) {
				outputs[0] = (!!i) & (!!j);
			}
//			std::cout << i << " " << j << " = " << outputs[0] << std::endl;
			set.push_back(inputs, outputs);
		}
	}

//	std::cout << "training: " << std::endl;
	nn.train(&set, epochs);
//	std::cout << "test: " << std::endl;
	nn.test(&set);
	nn.dumpLayers();
}

int main(int argc, char **argv) {
	train_logic_function("xor", "Sigmoid", std::vector<int> {4}, 10000, 0.7f);
	train_logic_function("xor", "ReLu", std::vector<int> {8, 8}, 100000, 0.02f);
	train_logic_function("xor", "ReLuLeaky",  std::vector<int> {8, 8}, 100000, 0.02f);
	return 0;
};
