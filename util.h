#ifndef __UTIL_H__
#define __UTIL_H__

#include <iostream>

class util {
public:
	static double rand(double start, double end);
	static int rand(int start, int end);
	static void shuffle_array(int *arr, int size);
};

#endif
