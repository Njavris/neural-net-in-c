#include "util.h"
#include <cstdlib>
#include <ctime>
#include <iostream>

double util::rand(double start, double end) {
	double ret, range = end - start;
	static bool init = false;
	if (!init) {
		std::srand(std::time(nullptr));
		init = true;
	}
	ret = (double)(std::rand() * range) / (double)RAND_MAX;
	ret += (double)start;
	return ret;
}

int util::rand(int start, int end) {
	int ret, range = end - start;
	static bool init = false;
	if (!init) {
		std::srand(std::time(nullptr));
		init = true;
	}
	ret = std::rand() % range;
	ret += start;
	return ret;
}

void util::shuffle_array(int *arr, int size) {
	if (size <= 1)
		return;
	for (int i = 0; i < size / 2; i++) {
		int tmp = arr[i];
		int j = util::rand(0, size);
		arr[i] = arr[j];
		arr[j] = tmp;
	}
}
