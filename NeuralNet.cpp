#include "NeuralNet.h"
#include "util.h"

double *NeuralNet::compute(double *in) {
	double *out = nullptr;
	Layer *last = layers.back();
	std::list<Layer *>::iterator it = layers.begin();
	if (!linked)
		linkLayers();

	(*it)->setInputs(in);
	for (it++; it != layers.end(); it++) {
		(*it)->forwardPropagate(*std::prev(it, 1),
					*std::next(it, 1));
	}

	if (last)
		out = last->getOutputs();
	return out;
}

void NeuralNet::linkLayers(void) {
	std::list<Layer *>::iterator it = layers.begin();
	double *prev = (*it)->getOutputs();
	
	for (it++; it != layers.end(); it++) {
		(*it)->setInputs(prev);
		prev = (*it)->getOutputs();
	}
	linked = true;
}

void NeuralNet::adjustWeights(double *expectedOut) {
	std::list<Layer *>::reverse_iterator it = layers.rbegin();
	(*it)->backwardPropagate(*std::prev(it, 1), nullptr, expectedOut);
	for (it++; it != layers.rend(); it++) {
		Layer *next = *std::prev(it, 1);
		Layer *prev = *std::next(it, 1);
		if (prev == *layers.begin())
			prev = nullptr;
		(*it)->backwardPropagate(prev, next, nullptr);
	}
}

void NeuralNet::train(TrainingSet *set, int epochs) {
	int *order = new int[set->size()];
	for (int i = 0; i < set->size(); i++)
		order[i] = i;

	for (int i = 0; i < epochs; i++) {
		util::shuffle_array(order, set->size());
		for (int j = 0; j < set->size(); j++) {
			int it = order[j];
			double *expected = set->getEntry(it)->second;
			resetLayers();
			compute(set->getEntry(it)->first);
			adjustWeights(expected);
		}
	}

	delete []order;
}

void NeuralNet::test(TrainingSet *set) {
	std::cout << std::fixed;
	for (int i = 0; i < set->size(); i++) {
		double *in = set->getEntry(i)->first;
		double *expected = set->getEntry(i)->second;
		double *out = compute(in);
		std::cout << "\tin: ";
		for (int j = 0; j < set->getIn(); j++)
			std::cout << in[j] << " ";
		std::cout << "\texpected: ";
		for (int j = 0; j < set->getOut(); j++)
			std::cout << expected[j];
		std::cout << "\tgot: ";
		for (int j = 0; j < set->getOut(); j++) {
			double delta = fabs(out[j] - expected[j]);
			std::cout << out[j] << "\tdelta: " << delta;
		}
	
		std::cout << std::endl;	
	}
}
